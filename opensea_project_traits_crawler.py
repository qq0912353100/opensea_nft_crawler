import os
import pandas as pd
import requests
import re
import bs4
import numpy as np
import time
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

PROJECT_NAME = 'PhantaBear'
CONTRACT_ADDRESS = '0x67d9417c9c3c250f61a83c7e8658dac487b56b09'
OPENSEA_URL = 'https://opensea.io'
# OPENSEA PROJECT COLLECTION PAGE
PROJECT_URL = OPENSEA_URL+'/collection/'+str.lower(re.sub(" ", "-", PROJECT_NAME))+'?search[sortBy]=LAST_SALE_DATE'



def web_driver_setting():
    options = Options()
    options.add_argument(
        'user-agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0"')
    options.add_argument("--disable-notifications")
    # options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    profile = webdriver.FirefoxProfile()
    profile.set_preference('dom.webdriver.enabled', False)
    options.add_argument('--no-sandbox')
    options.add_argument('--marionette')
    options.add_argument('--window-size=800x600')
    options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Firefox(options=options, firefox_profile=profile)
    return driver


def check_reveal(soup):
    img_url_in_website = soup.find("div", class_='Blockreact__Block-sc-1xf18x6-0 dBFmez AssetsSearchView--assets')
    img_url_in_website = img_url_in_website.find_all('img', class_='Image--image')
    img_url_in_website = [u['src'] for u in img_url_in_website]
    if len(np.unique(img_url_in_website)) != len(img_url_in_website):
        return False
    else:
        return True


def check_project_info(soup):
    target_div = soup.find("div", class_='Blockreact__Block-sc-1xf18x6-0 karjuF')
    target_div = target_div.find_all("div", class_='Overflowreact__OverflowContainer-sc-10mm0lu-0 gjwKJf')
    if len(target_div) == 4:
        total_items = target_div[0]
        total_owners = target_div[1]
        floor_price = target_div[2]
        volume_traded = target_div[3]
        return total_items, total_owners, floor_price, volume_traded
    else:
        return None


def tag_to_text(tags):
    return [t.text for t in tags]


def match_rarity(text):
    return float(re.findall("(.*)% have this trait", text)[0])


def rarity_ranking(CONTRACT_ADDRESS, total_items):
    save_path = './nft_project/{}'.format(PROJECT_NAME)
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    driver = web_driver_setting()
    asset_url = OPENSEA_URL + '/assets/' + CONTRACT_ADDRESS+'/'
    for i in range(1, total_items+1):
        if not os.path.exists(os.path.join(save_path, '{}_{}.csv'.format(PROJECT_NAME, str(i)))):
            try:
                driver.get(asset_url+str(i))
                buttons = driver.find_elements_by_xpath("//button[@class='UnstyledButtonreact__UnstyledButton-sc-ty1bh0-0 btgkrL BasePanel--header Panel--header']")
                buttons[1].click()
                response = driver.page_source
                soup = bs4.BeautifulSoup(response, 'lxml')
                traits = soup.find('div', class_='Panel--isContentPadded item--properties')
                property_type = tag_to_text(traits.find_all('div', class_='Property--type'))
                property_value = tag_to_text(traits.find_all('div', class_='Property--value'))
                property_rarity = tag_to_text(traits.find_all('div', class_='Property--rarity'))
                property_rarity_num = [match_rarity(r) for r in property_rarity]
                trade_price = soup.find("div", class_='Overflowreact__OverflowContainer-sc-10mm0lu-0 gjwKJf Price--amount').text.strip()
                project_data = pd.DataFrame({'token_id': i, 'property_type': property_type, 'property_value': property_value, 'property_rarity': property_rarity, 'score': 1000*(1/np.mean(property_rarity_num)), 'price': trade_price})
                project_data.to_csv(os.path.join(save_path, '{}_{}.csv'.format(PROJECT_NAME, str(i))))
            except:
                project_data = pd.DataFrame({'token_id': i, 'property_type': '', 'property_value': '',
                                               'property_rarity':'',
                                               'score': 0, 'price': trade_price})
                project_data.to_csv(os.path.join(save_path, '{}_{}.csv'.format(PROJECT_NAME, str(i))))
    driver.close()
    return project_data


if __name__ == '__main__':
    driver = web_driver_setting()
    driver.get(PROJECT_URL)
    for i in range(500, 10000, 500):
        js = 'var action=document.documentElement.scrollTop={}'.format(str(i))
        driver.execute_script(js)
    time.sleep(5)
    response = driver.page_source
    driver.close()
    soup = bs4.BeautifulSoup(response, 'lxml')
    if check_reveal(soup):
        # 可以配合通知系統
        print('revealing')
        # 開始做篩選跟排名
        total_items, total_owners, floor_price, volume_traded = tag_to_text(check_project_info(soup))
        if 'K' in total_items:
            total_items = float(re.findall('(.*)?K', total_items)[0]) * 1000
        else:
            total_items = float(re.findall('(.*)?K', total_items)[0])
        rarity_ranking(CONTRACT_ADDRESS, int(total_items))
    else:
        # 可以配合通知系統
        print('unrevealed')
