import numpy as np
import requests
import configparser
import time
import re
import numpy as np


def read_auth_key():
    config = configparser.ConfigParser()
    config.sections()
    config.read('config.ini')
    auth_key = config['OPENSEA']['auth_key']
    return auth_key


def limit_request_id_counts():
    config = configparser.ConfigParser()
    config.sections()
    config.read('config.ini')
    limit = int(config['OPENSEA']['limit'])
    return limit


def get_asset_contract_address():
    config = configparser.ConfigParser()
    config.sections()
    config.read('config.ini')
    asset_contract_address = int(config['USER_SETTING']['asset_contract_address'])
    asset_contract_address = str.lower(re.sub(" ", "", asset_contract_address))
    asset_contract_address = asset_contract_address.split(',')
    return asset_contract_address


def get_token_info(token_ids, asset_contract_address):
    """
    :param token_ids: str
    :param asset_contract_address: str
    :return: response as json
    """
    # Retrieving assets
    # dict_keys(['id', 'token_id', 'num_sales', 'background_color', 'image_url', 'image_preview_url',
    # 'image_thumbnail_url', 'image_original_url', 'animation_url', 'animation_original_url', 'name', 'description',
    # 'external_link', 'asset_contract', 'permalink', 'collection', 'decimals', 'token_metadata', 'owner',
    # 'sell_orders', 'creator', 'traits', 'last_sale', 'top_bid', 'listing_date', 'is_presale',
    # 'transfer_fee_payment_token', 'transfer_fee'])
    time_start = time.time()
    if len(token_ids) > limit_request_id_counts():
        return print('max request token_ids are {}'.format(str(limit_request_id_counts())))
    else:
        url_t = ''
        for i in token_ids:
            url_t += 'token_ids={}&'.format(str(i))
        auth_key = read_auth_key()
        url = "https://api.opensea.io/api/v1/assets?{}asset_contract_address={}&order_direction=desc&offset=0&limit={}".\
            format(url_t, asset_contract_address, str(limit_request_id_counts()))
        headers = {"X-API-KEY": auth_key}
        response = requests.request("GET", url, headers=headers, verify=False)
        response = response.json()
    time_end = time.time()
    print('Search Done cost {}s'.format(str(time_end-time_start)))
    return response


def check_project_items(contract_name):
    collection_slug = str.lower(re.sub(" ", "-", contract_name))
    auth_key = read_auth_key()
    url = 'https://api.opensea.io/api/v1/collection/{}'.format(collection_slug)
    headers = {"X-API-KEY": auth_key}
    response = requests.request("GET", url, headers=headers)
    response = response.json()
    counts = int(response['collection']['stats']['count'])
    return counts


def check_revealing(counts, asset_contract_address):
    # 對counts 做切割分組
    token_ids = list(range(1, counts+1))
    revealing = False
    while len(token_ids) >= limit_request_id_counts():
        pop_token_ids = []
        for i in range(1, limit_request_id_counts() + 1):
            pop_token_ids.append(token_ids.pop(1))
        response = get_token_info(pop_token_ids, asset_contract_address)
        for i in range(len(response['assets'])):
            if response['assets'][i]['traits']:
                revealing = True
    if revealing:
        return True
    else:
        print('contract not revealing')
        return False


