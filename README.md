# OPENSEA_NFT_CRAWLER



## Description

- This project aims to demonstrate how to use Selenium to operate Webdriver and has two main objectives:

1. To crawl the NFTs currently being revealed by the project and extract information on their rarity and floor prices. This data will enable the identification of NFTs with high rarity and low prices that can be purchased and resold for a profit.

2. To crawl the NFTs that have already been revealed and compare their prices with those of the NFTs currently being revealed. This information will be used to identify any price differentials that can be exploited for profit
